# VanitySearch-docker

This is a `Dockerfile` for the [VanitySearch][1] utility which finds bitcoin addresses with wanted prefix, either legacy (_Base58_ starting with _1_), segwit legacy-compatible (_Base58_ starting with _3_) or native segwit addresses (starting with _bc1_).

Please refer to [VanitySearch][1] documentation for the utility commands.

## Build

Because we're speaking of generating both **private** and public keys, do **not**
trust any binary tool, image or website for this task. Building the image takes a
couple of minutes but is a simple task:

```
$ docker build -t . whatever/vanitysearch
```

## Usage

Just because we _can_ read the code doesn't mean we do it, how can we be sure
there's not a clever backdoor somewhere in this code? A radical solution consists
on cutting the container from the Internet, this is done with the `--net none` flag.

### nvidia GPU accelerated search

For this to work, you probably want to modify the following `Dockerfile` line:

```
$ make gpu=1 ccap=52 all
```

With a value that suits your card. Mine is a _GTX 970_ which we can see [here](https://en.wikipedia.org/wiki/CUDA#Supported_GPUs) has its _Compute Capability_ version at `5.2`, so I set `ccap=52`.

```
 $ docker run --rm --net none \
	--device=/dev/nvidiactl --device=/dev/nvidia-uvm --device=/dev/nvidia0 \
	whatever/vanitysearch -stop -gpu bc1qyeah
```

### CPU search

```
$ docker run --rm --net none whatever/vanitysearch -stop bc1qyeah
```

## Tip

Mandatory BTC address `bc1q33m33lnp92le4h8aq2wwlp5xe2kjk3yg922jw7`

[1]: https://github.com/JeanLucPons/VanitySearch
