FROM debian:stretch-slim
LABEL maintainer="docker@esunix.com"

ENV GIT_URL https://github.com/JeanLucPons/VanitySearch

RUN echo 'debconf debconf/frontend select Noninteractive' | \
    debconf-set-selections && \
    echo keyboard-configuration \
        keyboard-configuration/layout select 'English (US)' | \
        debconf-set-selections && \
    echo keyboard-configuration keyboard-configuration/layoutcode select 'us' | \
        debconf-set-selections && \
    echo "deb http://deb.debian.org/debian stretch main contrib non-free" > \
        /etc/apt/sources.list.d/debian.list && \
    apt-get -y update && \
    apt-get -y --no-install-recommends install build-essential \
        git ca-certificates
RUN apt-get -y --no-install-recommends install nvidia-cuda-dev \
        nvidia-cuda-toolkit nvidia-driver
# CUDA nvcc needs gcc < 5, fortunately this trickery with jessie works
RUN echo "deb http://deb.debian.org/debian jessie main" > \
        /etc/apt/sources.list.d/jessie.list && \
    apt-get -y update && \
    apt-get -y install g++-4.9 && \
    apt-get -y clean
RUN cd /root && git clone $GIT_URL && cd /root/VanitySearch && \
    sed -Ei -e 's,^NVCC.*,NVCC\ =\ /usr/bin/nvcc,' -e 's,-4.8,-4.9,' Makefile && \
    cat Makefile && \
    make gpu=1 ccap=52 all

ENTRYPOINT ["/root/VanitySearch/VanitySearch"]
